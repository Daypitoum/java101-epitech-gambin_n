<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:url value="/" var="webappRoot">
</c:url>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${webappRoot}/resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="${webappRoot}/resources/bootstrap-theme.min.css"
	rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liste des cours</title>
</head>
<body>

	<table class="table">
		<tr>
			<th>Titre du Cours</th>
			<th>Prof</th>
		</tr>
		<c:forEach var="cours" items="${courses}">
			<tr>

				<td><c:out value="${cours.name}"></c:out></td>

				<td><a href="${webappRoot}/teacher/${cours.teacher.id}"><c:out
							value="${cours.teacher.name}" /></a></td>
				<td><a href="${webappRoot}/deletecourse/${cours.id}/confirm">Delete Course</a></td>
			</tr>
		</c:forEach>
	</table>

	<ul>
		<li><a href="nouveau-cours">add a new course</a></li>
		<li><a href="teachers">show all teachers</a></li>
		<li><a href="unemployed-teachers">show enemployed teachers</a></li>
	</ul>


</body>
</html>