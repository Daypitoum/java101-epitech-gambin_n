package net.epitech.java.td.controller.form;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class CourseFormElement {

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDayInweek() {
		return dayInweek;
	}

	public void setDayInweek(String dayInweek) {
		this.dayInweek = dayInweek;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@NotEmpty
	String name;
	@NotEmpty
	String dayInweek;
	@NotEmpty
	String dateStart;
	@Min(value = 0)
	@Max(value = 8)
	Integer duration;

}
