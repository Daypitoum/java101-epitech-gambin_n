package net.epitech.java.td.controller;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import net.epitech.java.td.controller.form.CourseFormElement;
import net.epitech.java.td.controller.form.TeacherFormElement;
import net.epitech.java.td.domain.Course;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.FailedToParseDayInWeekException;
import net.epitech.java.td.repositories.CourseRepository;
import net.epitech.java.td.repositories.TeacherRepositoy;
import net.epitech.java.td.service.EpitechService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

//je suis une class de type controller dans spring = je peux être injectée et je fais office de servlet.
@Controller
public class RouteController {

	@Inject
	private EpitechService service;

	@Inject
	TeacherRepositoy teacherRepo;

	@RequestMapping(value = { "/cours", "/" }, method = { RequestMethod.GET })
	public String getCourses(ModelMap model) throws IOException {

		model.addAttribute("courses", service.getCourses());
		return "courses";
	}
	
	@RequestMapping(value = { "/teachers"}, method = { RequestMethod.GET })
	public String getTeachers(ModelMap model) throws IOException {

		model.addAttribute("teachers", teacherRepo.findAll());
		return "teachers";
	}

	@RequestMapping(value = { "/deletecourse/{id}/confirm"}, method = { RequestMethod.GET })
	public String deleteCourse(ModelMap model, @PathVariable("id") Integer idCourse) throws IOException {
		model.addAttribute("cours", service.getCourseById(idCourse));
		return "deletecourse";
	}
	
	@RequestMapping(value = { "/deletecourse/{id}"}, method = { RequestMethod.GET })
	public String performDeleteCourse(ModelMap model, @PathVariable("id") Integer idCourse) throws IOException {
		try {
			service.deleteCourse(idCourse);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "courses";
	}

	
	@RequestMapping(value = { "/unemployed-teachers"}, method = { RequestMethod.GET })
	public String getUnemployedTeachers(ModelMap model) throws IOException {

		model.addAttribute("teachers", service.getTeacherWithNoCourses());
		return "teachers";
	}

	@RequestMapping(value = "/teacher/{id}")
	public String coursesProfessor(ModelMap model,
			@PathVariable("id") Integer idTeacher) throws IOException {
		
		
		Teacher theone = teacherRepo.findOne(idTeacher);
		model.addAttribute("teacher", theone);
		List<Course> courses = theone.getCourses();
		model.addAttribute("courses", courses);
		return "teacher";
	}

	@RequestMapping(value = "/nouveau-cours")
	public String addNewCourses(ModelMap model) throws IOException {

		model.addAttribute("course", new CourseFormElement());
		return "newcourse";
	}

	@RequestMapping(value = "/nouveau-teacher")
	public String addNewTeachers(ModelMap model) throws IOException {

		model.addAttribute("teacher", new TeacherFormElement());
		return "newteacher";
	}

	@RequestMapping(value = "/teacher", method = { RequestMethod.POST })
	public String addNewTeacher(
			@Valid @ModelAttribute("teacher") TeacherFormElement teacher,
			BindingResult result, HttpServletRequest request)
			throws IOException {

		if (result.hasErrors()) {
			return "newteacher";
		}

		// TODO: pass those to service layer to create course element
		try {
			service.addTeacher(teacher.getName(), teacher.getEmail());
		} catch (Exception e) {
			result.rejectValue("email", "bademail", "This teacher exists");
			return "newteacher";
		}
		return "redirect:/";
	}

	@RequestMapping(value = "/course", method = { RequestMethod.POST })
	public String addNewCourse(
			@Valid @ModelAttribute("course") CourseFormElement course,
			BindingResult result, HttpServletRequest request)
			throws IOException {

		if (result.hasErrors()) {
			return "newcourse";
		}

		try {
			service.addCourse(course.getName(), course.getDayInweek(),
					course.getDuration());
		} catch (FailedToParseDayInWeekException e) {
			result.rejectValue("dayInweek", "baddayInweek",
					"Failed to parse Day in Week");
			return "newcourse";
		}

		return "redirect:/";
	}

	@RequestMapping(value = "/test")
	public ModelAndView newCourse(HttpServletResponse response)
			throws IOException {

		return new ModelAndView("test", "command", new Course());
	}

	@RequestMapping("/planning")
	public String getPlanning(ModelMap model) throws IOException {

		model.addAttribute("courses", service.getCourses());

		return "planning";
	}
}
