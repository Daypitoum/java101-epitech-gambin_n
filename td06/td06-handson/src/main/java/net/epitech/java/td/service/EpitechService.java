package net.epitech.java.td.service;

import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.stereotype.Service;

import net.epitech.java.td.domain.Course;
import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.exception.FailedToParseDayInWeekException;

//TODO: interface must be throughly documented
public interface EpitechService {
	public Collection<Course> getCourses();
	
	public Course getCourseById(Integer Id);
	
	public Collection<Teacher> getTeacherWithNoCourses();

	public Collection<Teacher> getTeachers();

	// public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration
	// d);

	public Course addCourse(String name, String date, Integer duration) throws FailedToParseDayInWeekException;

	public void deleteCourse(Integer id) throws Exception;

	public Teacher addTeacher(String name, String mail) throws Exception;

	public void deleteTeacher(Integer id) throws Exception;
}