package net.epitech.java.td.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

//Cette classe est là pour configurer Spring
@Configuration
// voici la package de base dans lequel je vais chercher les classes à injecter
@ComponentScan(basePackages = "net.epitech.java.td")
// indique que la configuration se fait dans un contexte MVC. Nécessaire pour
// l'utilisation de la méthode addResourceHandlers
@EnableWebMvc
// on importe la configuration de la persistance
@Import(PersistenceConfiguration.class)
public class MvcConfiguration extends WebMvcConfigurerAdapter {

	// Je serai utilisé dès qu'une classe aura besoin d'un objet de type
	// ViewResolver
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	// permet de servir des resources statiques par tomcat
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations(
				"/resources/");
	}

}
